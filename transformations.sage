var('x, y, a, b, c, d')


def trig_identities(c1):
    c2 = c1
    c2 = c2.subs(cos(-a)==cos(a))
    c2 = c2.subs(sin(-a)==-sin(a))

    c2 = c2.subs(cos(a)^2 - sin(a)^2 == cos(2*a))
    c2 = c2.subs(sin(a)^2 - cos(a)^2 == -cos(2*a))

    c2 = c2.subs(2 * sin(a) * cos(a) == sin(2*a))
    c2 = c2.subs(-2 * sin(a) * cos(a) == -sin(2*a))

    c2 = c2.subs(sin(a)^2 + cos(a)^2 == 1)

    return c2

# mm - list / matrix
# return - list
def validate_mat(mm):
    msg = "validate_mat: Invalid matrix <" + str(mm) + ">"
    try:
        if type(mm) != list:
            mm = mm.rows()
        ll = len(mm)
        rr = list()
        for row in mm:
            if ll != len(row):
                raise TypeError()
            cc = list()
            for col in row:
                cc.append(col)
            rr.append(cc)
        return rr
    except AttributeError:
        print(msg, "(AttributeError)")
    except TypeError:
        print(msg, "(TypeError)")


# mm - list / matrix
# return - matrix
def mat(mm):
    mm = validate_mat(mm)
    ll = len(mm)
    for row in range(0, ll):
        for col in range(0, ll):
            mm[row][col] = trig_identities(mm[row][col])
    return matrix(mm)


# point
def P(x, y):
    return vector([x, y, 1])


# Translation
def TT(x=0, y=0):
    return mat([[1, 0, x],[0, 1, y], [0, 0, 1]])


# Rotation
def RR(x=0, y=0, a=0):
    if x == 0 and y == 0:
        return mat([[cos(a), -sin(a), 0],[sin(a), cos(a), 0], [0, 0, 1]])
    return mat( TT(x, y) * RR(0, 0, a) * TT(-x, -y) )


# Reflection
def Sl(a=0, b=0, c=0):
    if a == 0 and b == 0:
        raise RuntimeError("Invalid line")

    # across x axis
    if a == 0 and c == 0:
        return mat([[1, 0, 0],[0, -1, 0], [0, 0, 1]])

    # across y axis
    if b == 0 and c == 0:
        return mat([[-1, 0, 0],[0, 1, 0], [0, 0, 1]])

    # across y = k
    if a == 0:
        return mat( TT(0, -c/b) * Sl(0, 1, 0) * TT(0, c/b) )

    # across x = k
    if b == 0:
        return mat( TT(-c/a, 0) * Sl(1, 0, 0) * TT(c/a, 0) )


#############################################
#             utility functions             #
#############################################

# Reflection across y=k
def Syk(k=0):
    return mat( Sl(0, 1, -k) )

# Reflection across x=k
def Sxk(k=0):
    return mat( Sl(1, 0, -k) )

# Reflect across line represented with:
#   d       - distance d from origin
#   alpha   - angle a between d and positive x axis
def Sl1(d=0, alpha=0):
    #return mat( RR(0, 0, alpha) * Sxk(d) * RR(0, 0, -alpha) )
    return mat( RR(0, 0, alpha) * Sl(1, 0, -d) * RR(0, 0, -alpha) )


# TODO merge this into Sl
def Sl2(m=0, n=0, hold=False):
    if n != 0:
        return mat(Tg(0, -n) * Sl(m, 0) * tg(0, n))
    if m == 0:
        return Sx(0)
    mm = mat(R(0, 0, a) * Sx(0) * R(0,0, -a))
    if type(m) != Expression or m.has(a) == False:
        alpha = arctan(m)
        mm = mm.subs(a == alpha)
    return mm

