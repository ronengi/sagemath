# convert line representations

# From:     a*x + b*y + c == 0
# To:       d      - distance from origin
#           alpha  - angle between d and positive x axis
def ll_abc_da(abc):
    try:
        if len(abc) != 3:
            raise TypeError()
        a = abc[0]
        b = abc[1]
        c = abc[2]
        alpha = arctan(b/a)
        d = -(c/b)*sin(alpha)
        # print("d==", N(d),", alpha==", N(alpha * 180/pi), sep='')
        return [d, alpha]
    except TypeError:
        print("Line: invalid arguments")


# var('x, y, a, b, c, d')

# if True:    
#     raise RuntimeError("something happened")

#     try:
#         ll = len(mm.rows())
#         for row in range(0, ll):
#             if ll != len(mm[row]):
#                 raise TypeError()
#     except TypeError:
#         print("validate_mat: Invalid matrix <", mm, ">")



# # y = m*x + n
# def ll_mn(m=0, n=0):
#     return
# 
# # d, alpha
# def ll_daf(d=0, a=0):
#     return
# # y=k
# def ll_yk(k==0):
#     return
#     
# # x=k
# def ll_xk(k==0):
#     return
# 



#   - distance d from origin
#   - angle a between d and positive x axis
# def Sl1(d=0, a=0):
#     if a == 0 and d == 0:
#         return Syk(k=0)
#     return Sl(cos(a), sin(a), -d)


# TODO merge this into Sl
# def Sl2(m=0, n=0, hold=False):
#     if n != 0:
#         return mat(Tg(0, -n) * Sl(m, 0) * tg(0, n))
#     if m == 0:
#         return Sx(0)
#     mm = mat(R(0, 0, a) * Sx(0) * R(0,0, -a))
#     if type(m) != Expression or m.has(a) == False:
#         alpha = arctan(m)
#         mm = mm.subs(a == alpha)
#     return mm

